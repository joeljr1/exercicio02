package main;

public class Book {
	private String author;
	private int edition;
	private int volume;

	public void setAuthor(String author) {
		this.author = author;
	}

	public void setEdition(int edition) {
		this.edition = edition;
	}

	public void setVolume(int volume) {
		this.volume = volume;
	}

	public int getEdition() {
		return edition;
	}

	public int getVolume() {
		return volume;
	}

	public String getAuthor() {
		return author;
	}
}
